<?php

namespace common\models;

use yii\helpers\ArrayHelper;

class NewsWithCategories extends News
{
    public $category_ids = [];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            // each category_id must exist in category table
            ['category_ids', 'each', 'rule' => [
                'exist', 'targetClass' => Category::className(), 'targetAttribute' => 'id'
            ]
            ],
        ]);
    }

    public function loadCategories()
    {
        $this->category_ids = [];
        if (!empty($this->id)) {
            $rows = NewsHasCategory::find()
                ->select(['category_id'])
                ->where(['news_id' => $this->id])
                ->asArray()
                ->all();
            foreach($rows as $row) {
                $this->category_ids[] = $row['category_id'];
            }
        }
    }

    public function saveCategories()
    {
        NewsHasCategory::deleteAll(['news_id' => $this->id]);
        if (is_array($this->category_ids)) {
            foreach($this->category_ids as $category_id) {
                $pc = new NewsHasCategory();
                $pc->news_id = $this->id;
                $pc->category_id = $category_id;
                $pc->save();
            }
        }
    }
}