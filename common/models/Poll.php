<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pool".
 *
 * @property string $id
 * @property string $question
 * @property string $img
 *
 * @property PollQuestion[] $poolQuestions
 */
class Poll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question', 'img'], 'required'],
            [['question', 'img'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Question',
            'img' => 'Img',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollQuestions()
    {
        return $this->hasMany(PollQuestion::className(), ['poll_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'id']);
    }

    public function getPollData($id)
    {
        $model = self::findOne(['id' => $id]);
        $data = [];
        $allVotes = 0;
        foreach ($model->pollQuestions as $q) {
            $allVotes += $q->votes;
        }
        foreach ($model->pollQuestions as $q) {
            $data[$q->id] = [
                'percentage' => $q->votes != 0 ? round(($q->votes / $allVotes) * 100, 4) : 0,
                'votes' => $q->votes
            ];
        }

        return $data;
    }
}
