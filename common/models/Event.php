<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "event".
 *
 * @property string $id
 * @property string $name
 * @property integer $date
 * @property string $place
 * @property string $description
 */
class Event extends \yii\db\ActiveRecord
{
    public $month;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_from', 'place', 'description', 'img'], 'required'],
            [['name', 'place', 'description', 'date_from', 'date_to', 'month'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date' => 'Date',
            'place' => 'Place',
            'description' => 'Description',
        ];
    }

    public function beforeSave($insert)
    {
        $this->date_from = Yii::$app->formatter->asTimestamp($this->date_from);
        $this->date_to = Yii::$app->formatter->asTimestamp($this->date_to);
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->date_from = Yii::$app->formatter->asDate($this->date_from, 'dd.MM.y');
        if ($this->date_to == 0 || $this->date_to == null) {
            $this->date_to = null;
        } else {
            $this->date_to = Yii::$app->formatter->asDate($this->date_to, 'dd.MM.y');
        }
    }

    public function getClosest($limit = 2)
    {
        return self::find()
            ->where('date_to > ' . time())
            ->orderBy('date_to ASC')
            ->limit($limit)
            ->all();
    }

    public function getEventsWithMonths()
    {
        $modelsWithMonths = [];

        $models = self::find()
            ->where(['>', 'date_from', time() - 3600 * 24 * 7])
            ->orderBy('date_from ASC')
            ->all();

        $grouped = [];
        foreach ($models as $m) {
            $month = date('m', strtotime($m->date_from)); // 1-12
            $monthStr = null;
            switch ($month) {
                case 1:
                    $monthStr = 'Styczeń'; break;
                case 2:
                    $monthStr = 'Luty'; break;
                case 3:
                    $monthStr = 'Marzec'; break;
                case 4:
                    $monthStr = 'Kwiecień'; break;
                case 5:
                    $monthStr = 'Maj'; break;
                case 6:
                    $monthStr = 'Czerwiec'; break;
                case 7:
                    $monthStr = 'Lipiec'; break;
                case 8:
                    $monthStr = 'Sierpień'; break;
                case 9:
                    $monthStr = 'Wrzesień'; break;
                case 10:
                    $monthStr = 'Październik'; break;
                case 11:
                    $monthStr = 'Listopad'; break;
                case 12:
                    $monthStr = 'Grudzień'; break;
            }
            $m->month = $monthStr;

            $grouped[$monthStr][] = $m;
        }

        return $grouped;
    }
}
