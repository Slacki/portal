<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "news".
 *
 * @property string $id
 * @property string $title
 * @property string $text
 * @property integer $date
 * @property integer $views
 * @property string $slug
 * @property string $user_id
 * @property string $img
 *
 * @property Category $category
 * @property User $user
 * @property NewsHasCategory[] $newsHasCategories
 * @property Category[] $categories
 */
class News extends \yii\db\ActiveRecord
{
    const STATUS_VERIFIED = 1;
    const STATUS_NOT_VERIFIED = 2;
    const STATUS_DRAFT = 3;

    public $fetchedNewsIds = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text', 'date', 'user_id', 'img', 'status'], 'required'],
            [['title', 'text'], 'string'],
            [['views', 'user_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'date' => 'Date',
            'views' => 'Views',
            'slug' => 'Slug',
            'user_id' => 'User ID',
            'category_id' => 'Category ID',
            'img' => 'Img'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsHasCategories()
    {
        return $this->hasMany(NewsHasCategory::className(), ['news_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('news_has_category', ['news_id' => 'id']);
    }

    public function getAttachments()
    {
        return $this->hasMany(NewsAttach::className(), ['news_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        $this->date = Yii::$app->formatter->asTimestamp($this->date);
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->date = Yii::$app->formatter->asDate($this->date, 'dd.MM.y');
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['categories'] = function () {
            $c = [];
            foreach ($this->categories as $category) {
                $c[] = $category->toArray();
            }
            return $c;
        };
        return $fields;
    }

    // used in backend
    public static function getAllNews($limit = 100)
    {
        $categories = self::find()->orderBy('date DESC')->limit($limit)->asArray()->all();
        $items = ArrayHelper::map($categories, 'id', 'title');

        return $items;
    }

    public function getSliderNews()
    {
        $models = null;
        $models = self::find()
            ->where('status = ' . self::STATUS_VERIFIED)
            ->with('categories')
            ->joinWith('attachments')->where(['attachment' => NewsAttach::POS_SLIDER])
            ->all();
        $excludeFromFill = [];

        foreach ($models as $m) {
            $excludeFromFill[] = $m->id;
        }
        $this->fetchedNewsIds = $excludeFromFill;

        return $models;
    }

    public function getNewestWithoutDuplicates($limit = 4)
    {
        $excludeFromFill = $this->fetchedNewsIds;

        $models = self::find()
            ->with('categories')
            ->orderBy('date DESC')
            ->where('status = ' . self::STATUS_VERIFIED)
            ->andWhere(['not in', News::tableName() . '.id', $excludeFromFill])
            ->limit($limit)
            ->all();

        foreach ($models as $m) {
            $excludeFromFill[] = $m->id;
        }
        $this->fetchedNewsIds = $excludeFromFill;
        return $models;
    }

    public function getWeekTop($limit = 5)
    {
        return self::find()
            ->where('status = ' . self::STATUS_VERIFIED)
            ->andWhere('date > ' . (string)(time() - (60 * 60 * 24 * 7)))
            ->orderBy('views DESC')->limit($limit)->all();
    }

    public function getRandomWithExclude(array $exclude = null, $limit = 4)
    {
        $models = self::find()
            ->where(['not in', self::tableName() . '.id', $exclude])
            ->andWhere(['status' => self::STATUS_VERIFIED])
            ->orderBy('rand()')
            ->limit($limit)
            ->all();

        $fill = abs(count($models) - $limit);
        if ($fill > 0) {
            $models = ArrayHelper::merge(
                $models,
                self::find()
                    ->orderBy('rand()')
                    ->andWhere(['status' => self::STATUS_VERIFIED])
                    ->limit($fill)
                    ->all()
                );
        }

        return $models;
    }

    public function getNewest($limit = 4)
    {
        return self::find()
            ->orderBy('date DESC')
            ->limit($limit)
            ->all();
    }

    public function getExcerpt()
    {
        $text = strip_tags($this->text);
        $pattern = '@\[poll_\d+\]@';
        $text = preg_replace($pattern, '', $text);

        $offset = 150;
        if ($l = strlen($text) < $offset) $offset = $l;

        $text = substr($text, 0, strpos($text, ' ', $offset));
        $text = rtrim($text, ' .');
        $text = trim($text);

        return $text;
    }
}
