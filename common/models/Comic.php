<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "comic".
 *
 * @property int $id
 * @property int $user_id
 * @property string $img
 * @property string $title
 * @property string $slug
 * @property int $date
 */
class Comic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comic';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img', 'date', 'title'], 'required'],
            [['img', 'title'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'img' => 'Image',
            'title' => 'Title',
            'slug' => 'Slug',
            'date' => 'Date',
        ];
    }

    public function beforeSave($insert)
    {
        $this->date = Yii::$app->formatter->asTimestamp($this->date);
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->date = Yii::$app->formatter->asDate($this->date, 'dd.MM.y');
        if ($this->date == 0 || $this->date == null) {
            $this->date = null;
        } else {
            $this->date = Yii::$app->formatter->asDate($this->date, 'dd.MM.y');
        }
    }
}
