<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pool_question".
 *
 * @property string $id
 * @property string $text
 * @property integer $votes
 * @property string $pool_id
 *
 * @property Pool $pool
 */
class PollQuestion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll_question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['poll_id'], 'required'],
            [['text'], 'string'],
            [['votes', 'poll_id'], 'integer'],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Poll::className(), 'targetAttribute' => ['poll_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'votes' => 'Votes',
            'pool_id' => 'Pool ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(Poll::className(), ['id' => 'poll_id']);
    }
}
