<?php

namespace common\models;

use yii\db\ActiveRecord;
use Yii;

class NewsAttach extends ActiveRecord
{
    const POS_SLIDER = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_attach';
    }

    public function rules()
    {
        return [
            [['news_id', 'attachment'], 'required'],
            [['news_id', 'attachment'], 'integer'],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'id']],
        ];
    }

    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}