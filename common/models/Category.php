<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property string $color
 *
 * @property News[] $news
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                'ensureUnique' => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 7, 'min' => 7],
            [['color'], 'match', 'pattern' => '/^#[a-fA-F0-9]{6}$/', 'message' => 'Zły format koloru'],
            [['nav'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
        ];
    }

    /**
     * Get all the available categories (*4)
     * @return array available categories
     */
    public static function getAvailableCategories()
    {
        $categories = self::find()->orderBy('name')->asArray()->all();
        $items = ArrayHelper::map($categories, 'id', 'name');

        return $items;
    }
}
