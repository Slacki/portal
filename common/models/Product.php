<?php

namespace common\models;

/**
 * This is the model class for table "product".
 *
 * @property string $id
 * @property string $pros
 * @property string $cons
 * @property integer $stars
 * @property string $img
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pros', 'cons', 'img'], 'string'],
            [['stars', 'img'], 'required'],
            [['stars'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pros' => 'Zalety',
            'cons' => 'Wady',
            'stars' => 'Stars',
            'img' => 'Img'
        ];
    }
}
