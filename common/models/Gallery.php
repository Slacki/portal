<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property string $id
 * @property string $news_id
 * @property string $event_id
 * @property string $user_id
 * @property string $img
 *
 * @property Event $event
 * @property News $news
 * @property User $user
 */
class Gallery extends \yii\db\ActiveRecord
{
    const PARENT_NEWS = 1;
    const PARENT_EVENT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'img', 'parent_id', 'parent_type'], 'required'],
            [['parent_id', 'parent_type', 'user_id'], 'integer'],
            [['img'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'parent_type' => 'Parent Type',
            'user_id' => 'User ID',
            'img' => 'Img',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
