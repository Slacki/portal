<?php

namespace frontend\components;

use yii\base\Widget;

class NewsTileWidget extends Widget
{
    public $model;
    public $colSize = 6;
    public $comic = false;

    public function run()
    {
        $view = $this->comic ? 'comicTile' : 'newsTile';
        return $this->render($view, [
            'model' => $this->model,
            'col' => $this->colSize,
        ]);
    }
}