<?php

namespace frontend\components;

use common\models\Product;
use yii\base\Widget;

class ProductWidget extends Widget
{
    public $id;

    protected $model;

    public function init()
    {
        $this->model = Product::findOne(['id' => $this->id]);
    }

    public function run()
    {
        if ($this->model == null) {
            return '<p></p>';
        }

        return $this->render('product', [
            'model' => $this->model,
        ]);
    }
}