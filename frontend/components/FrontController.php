<?php

namespace frontend\components;

use common\models\Category;
use yii\web\Controller;
use Yii;

class FrontController extends Controller
{
    /**
     * This is called before any action on frontend and takes care of all kinda stuff around the content
     */
    public function beforeAction($action)
    {
        $cache = Yii::$app->cache;
        $categories = $cache->get('layout-categories');

        if ($categories === false) {
            $categories = Category::find()->where(['nav' => 1])->all();
            $cache->set('layout-categories', $categories, 3600); // 1h
        }

        Yii::$app->view->params['categories'] = $categories;
        Yii::$app->view->params['news'] = false;

        return parent::beforeAction($action);
    }
}