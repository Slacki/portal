<?php
/* @var $this \yii\web\View */
?>

<div class="product-wrapper">
    <div class="product-bg" style="background: url('<?= $model->img; ?>');"></div>
    <h2>Ocena redakcji</h2>
    <div class="stars">
        <?php
        $stars = $model->stars;
        $full = (int)($stars / 2);
        $half = $stars % 2 == 1;
        $empty = 5 - $full;
        if ($half) {
            $empty -= 1;
        }

        for ($i = 0; $i < $full; $i++) {
            echo '<img src="' . Yii::getAlias('@web/img/star_full.svg') . '">';
        }
        if ($half) {
            echo '<img src="' . Yii::getAlias('@web/img/star_half.svg') . '">';
        }
        for ($i = 0; $i < $empty; $i++) {
            echo '<img src="' . Yii::getAlias('@web/img/star_empty.svg') . '">';
        }
        ?>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-6 vertical-line">
            <div class="col-header pros">
                zalety
                <img src="<?= Yii::getAlias('@web/img/product_pros_cons.svg'); ?>">
            </div>
            <ul>
                <?php
                foreach (explode(PHP_EOL, $model->pros) as $item) {
                    echo '<li>' . $item . '</li>';
                }
                ?>
            </ul>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="col-header cons">
                <img src="<?= Yii::getAlias('@web/img/product_pros_cons.svg'); ?>">
                wady
            </div>
            <ul>
                <?php
                foreach (explode(PHP_EOL, $model->cons) as $item) {
                    echo '<li>' . $item . '</li>';
                }
                ?>
            </ul>
        </div>
    </div>
</div>

<style>
    .product-wrapper {
        width: calc(100% - 30px);
        min-height: 500px;
        padding: 20px;
        margin: 30px auto;
        position: relative;
        z-index: 1;
    }

    .product-bg {
        background-position: center center !important;
        background-size: cover !important;
        background-repeat: no-repeat !important;
        position: absolute;
        width: 100%;
        height: 100%;
        filter: grayscale(50%);
        margin: -20px;
    }

    .product-bg:before {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: black;
        opacity: 0.7;
        content: '';
    }

    .product-wrapper h2 {
        font-size: 24px;
        font-weight: 700;
        text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.75);
        color: white;
        position: relative;
        margin-bottom: 50px;
        padding: 10px 20px;
        display: inline-block;
        background: #f98a2f;
        background: -moz-linear-gradient(left,  #f98a2f 0%, #f49a46 100%);
        background: -webkit-linear-gradient(left,  #f98a2f 0%,#f49a46 100%);
        background: linear-gradient(to right,  #f98a2f 0%,#f49a46 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f98a2f', endColorstr='#f49a46',GradientType=1 );
    }

    .col-header {
        font-size: 22px;
        color: white;
        font-weight: 700;
        text-align: center;
    }

    .col-header img {
        display: inline;
        height: 110px;
        margin-top: -15px;
        margin-left: -25px;
        margin-right: -25px;
    }

    .col-header.cons img {
        -webkit-transform: rotate(180deg);
        -moz-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        transform: rotate(180deg);
        margin-top: -3px;
        margin-bottom: -12px;
    }

    @media (min-width: 992px) {
        .product-wrapper .vertical-line {
            border-right: 1px solid white;
            padding-bottom: 40px;
            min-height: 350px;
            margin-bottom: 60px;
        }
    }

    .product-wrapper ul {
        margin: 20px 0 0 0;
        padding: 0;
    }

    .product-wrapper ul li {
        font-size: 18px;
        color: white;
        list-style: none;
        margin-left: 0;
        padding-left: 28px;
        text-indent: -25px;
        width: 90%;
        text-align: left;
    }

    .product-wrapper ul li:before {
        content: '';
        width: 10px;
        height: 10px;
        background: transparent;
        border: 2px solid #f4942d;
        border-radius: 50%;
        display: inline-block;
        margin: 0 15px 0 0;
    }

    .stars {
        display: inline-block;
        float: right;
        margin-top: 27px;
    }

    @media (max-width: 768px) {
        .stars {
            float: none;
            display: block;
            margin-top: 0;
        }

        .product-wrapper .row {
            padding-bottom: 30px;
        }
    }

    .stars img {
        display: inline-block;
        width: 30px;
        padding: 0;
        position: relative;
        margin: 0 5px;
    }
</style>