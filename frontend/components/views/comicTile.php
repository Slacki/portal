<?php
/* @var $this \yii\web\View */
/* @var $model \common\models\Comic */
?>

<div class="news col-xs-12 col-sm-<?= $col; ?>">
    <a class="news-link" href="<?= \yii\helpers\Url::to(['comic/view', 'id' => $model->id, 'slug' => $model->slug]) ?>">
        <div class="under-link-wrap">
            <h3 class="title"><?= $model->title ?></h3>
            <div class="news-img-wrapper"><div class="news-img" style="background-image: url('<?= $model->img ?>');"></div></div>
        </div>
    </a>
</div>