<?php
/* @var $this \yii\web\View */

$getDataUrl = \yii\helpers\Url::to(['poll/get-data', 'id' => $model->id]);
$questionUpUrl = \yii\helpers\Url::to(['poll/question-up']);
$_voted = null;
if ($voted) {
    $_voted = 'true';
} else {
    $_voted = 'false';
}

$js = <<<JS
voted = $_voted;
  
function displayData(_data) {
    $('.question').each(function (index) {
        var id = $(this).data('id');
        $(this).find('.percentage-bar').attr('style', 'width: ' + _data[id]['percentage'] + '%');
        
        perc = parseInt(_data[id]['percentage']);
        if (perc == 0) {
            perc = '<1';
        }
        $(this).find('.percentage').html(perc + '%, ' + _data[id]['votes'] + ' głosów');
    });
}

if (voted) {
    $('.poll-wrapper').addClass('voted');
    $.ajax({
        type: 'get',
        url: '$getDataUrl',
        success: function (r) {
            displayData(r);
        }
    });
}

$('.question').click(function () {
    if (voted) return;
    
    voted = true;
    $('.poll-wrapper').addClass('voted');
    $.ajax({
        type: 'post',
        url: '$questionUpUrl',
        data: {'id': $(this).data('id')}
    }).done(
        $.ajax({
            type: 'get',
            url: '$getDataUrl',
            success: function (r) {
                displayData(r);
            }
        })
    );

});

JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>

<div class="poll-wrapper">
    <div class="poll-bg" style="background: url('<?= $model->img; ?>');"></div>
    <h2><?= $model->question; ?></h2>
    <?php foreach ($model->pollQuestions as $q): if ($q->text == '') continue; ?>
        <div class="question" data-id="<?= $q->id; ?>">
            <p class="question-text">
                <?= $q->text; ?>
                <div class="percentage-bar"></div>
            </p>
            <p class="percentage pull-right"></p>
        </div>
    <?php endforeach; ?>
</div>

<style>
    .poll-wrapper {
        width: calc(100% - 30px);
        min-height: 400px;
        padding: 20px;
        margin: 30px auto;
        position: relative;
        z-index: 1;
    }

    .poll-bg {
        background-position: center center !important;
        background-size: cover !important;
        background-repeat: no-repeat !important;
        position: absolute;
        width: 100%;
        height: 100%;
        filter: grayscale(50%);
        margin: -20px;
    }

    .poll-bg:before {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: black;
        opacity: 0.7;
        content: '';
    }

    .poll-wrapper h2 {
        font-size: 24px;
        font-weight: 700;
        text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.75);
        color: white;
        position: relative;
        margin-bottom: 30px;
        padding: 10px 20px;
        display: inline-block;
        background: #f98a2f;
        background: -moz-linear-gradient(left,  #f98a2f 0%, #f49a46 100%);
        background: -webkit-linear-gradient(left,  #f98a2f 0%,#f49a46 100%);
        background: linear-gradient(to right,  #f98a2f 0%,#f49a46 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f98a2f', endColorstr='#f49a46',GradientType=1 );
    }

    .question {
        padding: 15px 20px 15px 20px;
        margin: 10px 0;
        position: relative;
        cursor: pointer;
        background: rgba(255, 255, 255, 0.2);
        z-index: 10;
    }

    .voted .question {
        cursor: default;
    }

    .percentage-bar {
        position: absolute;
        width: 0;
        height: 5px;
        background: coral;
        bottom: 0;
        left: 0;
        transition: all 1000ms linear;
    }

    .question:hover {
        background: rgba(255, 255, 255, 0.4);
    }

    .voted .question:hover {
        background: rgba(255, 255, 255, 0.2);
    }

    .question p {
        text-indent: 10px;
        color: white;
        position: relative;
        z-index: 11;
        padding: 0;
        margin: 0;
    }

    p.percentage {
        margin: -19px 0 0 0;
    }
</style>