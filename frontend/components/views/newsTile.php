<?php
/* @var $this \yii\web\View */
/* @var $model \common\models\News */
?>

<div class="news col-xs-12 col-sm-<?= $col; ?>">
    <a class="news-link" href="<?= \yii\helpers\Url::to(['news/view', 'id' => $model->id, 'slug' => $model->slug]) ?>">
        <div class="under-link-wrap">
            <div class="news-cats">
                <?php foreach ($model->categories as $c): ?>
                    <span<?= $c->color != '' ? ' style="color: white; background-color: ' . $c->color . '; border-color: ' . $c->color . ';"' : '' ?>><?= $c->name ?></span>
                <?php endforeach; ?>
            </div>
            <h3 class="title"><?= $model->title ?></h3>
            <div class="news-img-wrapper"><div class="news-img" style="background-image: url('<?= $model->img ?>');"></div></div>
        </div>
    </a>
</div>