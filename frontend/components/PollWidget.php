<?php

namespace frontend\components;

use common\models\Poll;
use yii\base\Widget;

class PollWidget extends Widget
{
    public $id;

    /**
     * @var Poll
     */
    protected $model;
    protected $data;
    protected $voted;

    public function init()
    {
        $this->model = Poll::findOne(['id' => $this->id]);
        if ($this->model == null) {
            return;
        }
        $this->data = $this->model->getPollData($this->id);
        $sessData = \Yii::$app->session->get('voted_polls');
        $this->voted = in_array($this->id, $sessData == null ? [] : $sessData);
    }

    public function run()
    {
        if ($this->model == null) {
            return '<p></p>';
        }

        return $this->render('poll', [
            'model' => $this->model,
            'data' => $this->data,
            'voted' => $this->voted,
        ]);
    }
}