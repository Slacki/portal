<?php

namespace frontend\controllers;

use common\models\Comic;
use frontend\components\FrontController;
use yii\data\Pagination;

class ComicController extends FrontController
{
    public function actionView($id, $slug = null)
    {
        $model = Comic::find()->where(['id' => $id])->one();
        if ($model == null) {
            return $this->goHome();
        }

        // if link without slug and slug exists in db (no infinite redirects)
        if ($slug == null && $model->slug != null) {
            return $this->redirect(['news', 'id' => $id, 'slug' => $model->slug]);
        }
        // if slugs doesnt match
        if ($slug != null && $model->slug != null) {
            if ($slug != $model->slug) {
                return $this->redirect(['news', 'id' => $id, 'slug' => $model->slug]);
            }
        }



        return $this->render('index', [
            'model' => $model,
            'next' => Comic::find()->where(['>', 'id', $id])->orderBy(['id' => SORT_ASC])->one(),
            'previous' => Comic::find()->where(['<', 'id', $id])->orderBy(['id' => SORT_DESC])->one(),
        ]);
    }

    public function actionList()
    {
        $query = Comic::find();
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 16]);
        $models = $query
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('list', [
            'models' =>  $models,
            'pagination' => $pagination,
        ]);
    }
}
