<?php

namespace frontend\controllers;

use common\models\Poll;
use common\models\PollQuestion;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class PollController extends Controller
{
    public function actionGetData($id)
    {
        $model = new Poll();
        $data = $model->getPollData($id); 
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; 
        \Yii::$app->response->data = $data;
        \Yii::$app->end();
    }

    public function actionQuestionUp()
    {
        if (!isset($_POST) && $_POST['id'] == null && !is_int($_POST['id'])) {
            return;
        }

        $votedPolls = \Yii::$app->session->get('voted_polls');
        if ($votedPolls == null) {
            $votedPolls = [];
        }
        if (in_array((string)$_POST['id'], $votedPolls)) {
            return;
        }

        /* @var $model PollQuestion */
        $model = PollQuestion::findOne(['id' => (int)$_POST['id']]);
        if ($model == null) {
            return;
        }
        $model->votes += 1;
        $model->save();

        \Yii::$app->session->set('voted_polls', ArrayHelper::merge($votedPolls, [$model->poll_id]));
    }
}