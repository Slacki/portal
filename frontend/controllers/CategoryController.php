<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\News;
use frontend\components\FrontController;
use yii\data\Pagination;

class CategoryController extends FrontController
{
    public function actionView($id, $slug = null)
    {
        $model = Category::find()->where(['id' => $id])->one();
        if ($model == null) {
            return $this->goHome();
        }

        // if link without slug and slug exists in db (no infinite redirects)
        if ($slug == null && $model->slug != null) {
            return $this->redirect(['category/view', 'id' => $id, 'slug' => $model->slug]);
        }
        // if slugs doesnt match
        if ($slug != null && $model->slug != null) {
            if ($slug != $model->slug) {
                return $this->redirect(['category/view', 'id' => $id, 'slug' => $model->slug]);
            }
        }

        $query = News::find()
            ->joinWith('categories')
            ->where(['category_id' => $id])
            ->andWhere(['status' => News::STATUS_VERIFIED]);
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 16]);
        $models = $query
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'categoryModel' => $model,
            'models' =>  $models,
            'pagination' => $pagination,
        ]);
    }
}