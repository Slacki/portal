<?php

namespace frontend\controllers;

use common\models\Event;
use common\models\Gallery;
use frontend\components\FrontController;

class EventController extends FrontController
{
    public function actionView($id, $slug = null)
    {
        $model = Event::find()->where(['id' => $id])->one();
        if ($model == null) {
            return $this->goHome();
        }

        // if link without slug and slug exists in db (no infinite redirects)
        if ($slug == null && $model->slug != null) {
            return $this->redirect(['news', 'id' => $id, 'slug' => $model->slug]);
        }
        // if slugs doesnt match
        if ($slug != null && $model->slug != null) {
            if ($slug != $model->slug) {
                return $this->redirect(['news', 'id' => $id, 'slug' => $model->slug]);
            }
        }

        $gallery = Gallery::find()
            ->where(['parent_id' => $id])
            ->andWhere(['parent_type' => Gallery::PARENT_EVENT])
            ->all();

        return $this->render('index', [
            'model' => $model,
            'gallery' => $gallery
        ]);
    }

    public function actionList()
    {
        $model = new Event();
        $data = $model->getEventsWithMonths();

        return $this->render('list', ['models' => $data]);
    }
}
