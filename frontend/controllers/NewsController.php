<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Gallery;
use common\models\News;
use frontend\components\FrontController;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class NewsController extends FrontController
{
    public function actionView($id, $slug = null)
    {
        /* @var $model News */
        $model = News::find()->where(['id' => $id])->one();
        if ($model == null) {
            return $this->goHome();
        }

        // if link without slug and slug exists in db (no infinite redirects)
        if ($slug == null && $model->slug != null) {
            return $this->redirect(['news/view', 'id' => $id, 'slug' => $model->slug]);
        }
        // if slugs doesnt match
        if ($slug != null && $model->slug != null) {
            if ($slug != $model->slug) {
                return $this->redirect(['news/view', 'id' => $id, 'slug' => $model->slug]);
            }
        }

        $this->view->params['news'] = true;
        $this->view->params['newsTitle'] = $model->title;
        $this->view->params['newsImg'] = $model->img;
        $this->view->params['newsId'] = $model->id;
        $this->view->params['newsSlug'] = $model->slug;
        $this->view->params['newsExcerpt'] = $model->getExcerpt();

        $model->views++;
        $model->save(false);

        // set this id of these news in session as viewed
        $viewedNews = \Yii::$app->session->get('viewed_news');
        if ($viewedNews == null) {
            $viewedNews = [];
        }
        if (!in_array($id, $viewedNews)) {
            \Yii::$app->session->set('viewed_news', ArrayHelper::merge($viewedNews, [$model->id]));
        }

        $gallery = \Yii::$app->cache->get('gallery-' . $id);
        if ($gallery === false) {
            $gallery = Gallery::find()
                ->where(['parent_id' => $id])
                ->andWhere(['parent_type' => Gallery::PARENT_NEWS])
                ->all();
            \Yii::$app->cache->set('gallery-' . $id, $gallery, 3600);
        }

        return $this->render('index', [
            'model' => $model,
            'nextNews' => $model->getNewest(4),
            'gallery' => $gallery,
        ]);
    }

    public function actionSearch()
    {
        if (!isset($_POST['search']) || $_POST['search'] == '') {
            return $this->goBack();
        }

        $model = new Category();
        $model->name = 'Wyniki wyszukiwania';

        $query = News::find()
            ->andFilterWhere(
                ['like', 'title', $_POST['search']],
                ['like', 'text', $_POST['search']],
                ['like', 'slug', $_POST['search']]
            )
            ->andWhere(['status' => News::STATUS_VERIFIED])
            ->orderBy('date DESC');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 16]);
        $models = $query
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('@frontend/views/category/index', [
            'categoryModel' => $model,
            'models' => $models,
            'pagination' => $pagination,
        ]);
    }
}
