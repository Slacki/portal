<?php
namespace frontend\controllers;

use common\models\Event;
use common\models\News;
use common\models\NewsAttach;
use frontend\components\FrontController;
use Yii;
use frontend\models\ContactForm;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class SiteController extends FrontController
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $newsModel = new News();
        $eventModel = new Event();

        // these dont affect global exclude
        $modelsWeekTop = $newsModel->getWeekTop(5);

        // these do, order matters
        $modelsSlider = $newsModel->getSliderNews();
        $modelsUnderSlider = $newsModel->getNewestWithoutDuplicates(4);
        $modelsNextToEvents = $newsModel->getNewestWithoutDuplicates(4);

        return $this->render('index', [
            'slider' => $modelsSlider,
            'underSlider' => $modelsUnderSlider,
            'weekTop' => $modelsWeekTop,
            'nextToEvents' => $modelsNextToEvents,
            'events' => $eventModel->getClosest(),
            'fetchedNewsIds' => $newsModel->fetchedNewsIds,
        ]);
    }

    public function actionMore($fetched, $amount = 6)
    {
        $model = new News();

        $fetchedArray = [];
        foreach (explode(',', $fetched) as $item) {
            $fetchedArray[] = (int)$item;
        }

        $data = [
            'data' => '',
            'fetchedNewsIds' => $fetched,
        ];

        $model->fetchedNewsIds = $fetchedArray;
        $modelsToRender = $model->getNewestWithoutDuplicates($amount);
        if ($modelsToRender != null) {
            $data['data'] = $this->renderPartial('newsRow', [
                'models' => $modelsToRender,
            ]);
        }
        
        $data['fetchedNewsIds'] = $model->fetchedNewsIds;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->data = $data;
        \Yii::$app->end();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['contactEmail'])) {
                Yii::$app->session->setFlash('sent', true);
            } else {
                Yii::$app->session->setFlash('sent', false);
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionPrivacyPolicy()
    {
        return $this->render('privacy-policy');
    }

    public function actionOffline()
    {
        echo 'Przerwa techniczna, wracamy za kilka minut';
        exit(0);
    }
}
