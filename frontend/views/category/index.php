<?php

/* @var $this yii\web\View */
use yii\web\View;
use frontend\components\NewsTileWidget;

$this->title = Yii::$app->name . ' · ' . $categoryModel->name;
\frontend\assets\SlickAsset::register($this);
?>

<div class="padded-multiline">
    <h1 class="gradient-blue-heading"><?= ucfirst($categoryModel->name) ?></h1>
</div>

<?php if (count($models) <= 0): ?>
    <p class="no-content">
        Nie znaleziono żadnych wyników.
    </p>
<?php endif; ?>

<section class="row section-category">
    <?php foreach ($models as $s) echo NewsTileWidget::widget(['model' => $s, 'colSize' => 4]); ?>
</section>

<?= \yii\widgets\LinkPager::widget([
    'pagination' => $pagination,
]); ?>