<?php

/* @var $this yii\web\View */
use yii\web\View;
use frontend\components\NewsTileWidget;

$this->title = $model->title . ' · GameLoad';

$this->registerJs("
if (window.matchMedia('(max-width: 992px)').matches) {
    $('.news-right').detach().appendTo('.news-head');
} else {
    $('.news-right').detach().appendTo('.news-head-img');
}
$(window).on('resize', function (e) {
    if (window.matchMedia('(max-width: 992px)').matches) {
        $('.news-right').detach().appendTo('.news-head');
    } else {
        $('.news-right').detach().appendTo('.news-head-img');
    }
});
$('.gallery').slick({
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    lazyLoad: 'progressive',
    variableWidth: true,
    focusOnSelect: true,
    arrows: false,
    dots: false, 
    responsive: [
        {
            breakpoint: 768,
            settings: {
                centerMode: false,
            }
        }
    ]
});
", View::POS_READY);

$this->render('../layouts/facebookSdk.php');

\frontend\assets\SlickAsset::register($this);
?>
<div id="fb-root"></div>

<div class="news-head">
    <div class="news-head-img" style="background-image: url('<?= $model->img ?>');">
        <div class="news-right">
            <div class="tags">
                <div class="news-cats">
                    <?php
                    if ($this->beginCache('news-categories-' . $model->id, ['duration' => 120])) {
                        foreach ($model->categories as $c) {
                            echo "<span>{$c->name}</span>";
                        }
                        $this->endCache();
                    }
                    ?>
                </div>
            </div>
            <div class="social">
                <div class="fb-like"
                    data-href="<?= Yii::$app->request->absoluteUrl; ?>"
                    data-width="400"
                    data-layout="box_count"
                    data-action="like"
                    data-size="large"
                    data-show-faces="false"
                    data-share="true">

                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<p class="news-author">
    <?php
    if ($this->beginCache('news-author-' . $model->id, ['duration' => 120])) {
        $text = \Yii::$app->formatter->asDate($model->date, 'dd.MM.y') . ', ';
        if ($model->user->first_name != null) {
            $text .= $model->user->first_name . ' ';
        }
        if ($model->user->nick != null) {
            $text .= '"' . $model->user->nick . '" ';
        }
        if ($model->user->last_name != null) {
            $text .= $model->user->last_name;
        }
        echo trim($text, ', ');

        $this->endCache();
    }
    ?>
</p>

<div class="news-content">
    <h1><?= $model->title ?></h1>
    <div class="news-h-separator"></div>

    <?= $this->render('../layouts/contentWithWidgets.php', ['model' => $model]) ?>
</div>

<?php if ($gallery != null) : ?>
    <div class="gallery">
        <?php foreach ($gallery as $g) : ?>
            <img data-lazy="<?= $g->img; ?>">
         <?php endforeach; ?>
    </div>
<?php endif; ?>

<?= $this->render('../layouts/disqus.php', ['model' => $model]); ?>
<?= $this->render('../layouts/facebookSocial.php') ?>

<div class="row next-news">
    <div class="news-h-separator"></div>
    <?php foreach ($nextNews as $s) : ?>
        <div class="next-news-tile col-md-6">
            <div class="img" style="background-image: url('<?= $s->img ?>');"></div>
            <a href="<?= \yii\helpers\Url::to(['news/view', 'id' => $s->id, 'slug' => $s->slug]) ?>">
                <h3><?= $s->title ?></h3>
                <p><?= $s->excerpt ?>...</p>
            </a>
            <div class="clearfix"></div>
        </div>
    <?php endforeach; ?>
</div>