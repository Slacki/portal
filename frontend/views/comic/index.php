<?php

/* @var $this yii\web\View */
use yii\web\View;

$this->title = $model->title . ' · GameLoad';

$this->registerJs("
$('.gallery').slick({
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    lazyLoad: 'progressive',
    variableWidth: true,
    focusOnSelect: true,
    arrows: false,
    dots: false, 
    responsive: [
        {
            breakpoint: 768,
            settings: {
                centerMode: false,
            }
        }
    ]
});", View::POS_READY);

$this->render('../layouts/facebookSdk.php');

?>
<div id="fb-root"></div>

<div class="news-content">
    <h1><?= $model->title ?></h1>
    <div class="news-h-separator"></div>

    <a href="<?= $model->img ?>">
        <img class="img-responsive" src="<?= $model->img ?>">
    </a>

    <div class="comic-buttons">
        <?php if ($previous !== null): ?>
        <a href="<?= \yii\helpers\Url::to(['comic/view', 'id' => $previous->id, 'slug' => $previous->slug]) ?>">
            <button class="big-button comic-button previous">Poprzedni</button>
        </a>
        <?php endif; ?>
        <?php if ($next !== null): ?>
        <a href="<?= \yii\helpers\Url::to(['comic/view', 'id' => $next->id, 'slug' => $next->slug]) ?>">
            <button class="big-button comic-button next">Następny</button>
        </a>
        <?php endif; ?>
    </div>
</div>
<div class="clearfix"></div>

<?= $this->render('../layouts/disqus.php', ['model' => $model]); ?>
<?= $this->render('../layouts/facebookSocial.php') ?>