<?php
use frontend\components\NewsTileWidget;
?>

<div class="col-xs-12">
    <div class="row">
        <?php 
        foreach ($models as $m) echo NewsTileWidget::widget([
            'model' => $m, 
            'colSize' => 4,
        ]); 
        ?>
    </div>
</div>