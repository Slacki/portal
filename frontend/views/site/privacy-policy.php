<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'GameLoad · Polityka prywatności';
?>

<div class="padded-multiline">
    <h1 class="gradient-blue-heading">Polityka prywatności</h1>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>

<p>GameLoad (CFA) szanuje prawo użytkowników do prywatności. Dba, ze szczególną troską, o ochronę ich danych osobowych oraz stosuje odpowiednie rozwiązania technologiczne zapobiegające ingerencji w prywatność użytkowników osób trzecich. Chcielibyśmy, by każdy internauta korzystający z naszych usług i odwiedzający nasze strony czuł się w pełni bezpiecznie, stąd wprowadzone, w niektórych serwisach, systemy rejestracji i logowania. Użytkownik, po zarejestrowaniu się, uzyskuje dostęp komentarzy itp. Rejestracja nie jest obowiązkowa.</p>

<h3>Cookies i inne podobne technologie</h3>

<p>
    Podmiotem zamieszczającym informacje w formie plików cookies (tzw. ciasteczek) i innych podobnych technologii w urządzeniu końcowym Użytkownika (np. komputerze, laptopie, smartfonie, Smart TV) oraz uzyskującym do nich dostęp jest GameLoad (CFA) z siedzibą w Stężycy.
</p>
<p>
    Zamieszczać informacje w urządzeniu końcowym użytkownika i korzystać z nich mogą także podmioty współpracujące z CFA., m.in. partnerzy świadczący usługi analityczne, reklamodawcy, twórcy aplikacji, sieciowe agencje reklamowe.
</p>
<p>
    Pliki cookies stanowią dane informatyczne, w szczególności pliki tekstowe, które przechowywane są w urządzeniu końcowym Użytkownika serwisów i aplikacji CFA. Cookies zazwyczaj zawierają nazwę domeny serwisu internetowego, z którego pochodzą, czas przechowywania ich na urządzeniu końcowym oraz unikalny numer.
</p>
<p>
    Pliki cookies nie służą identyfikacji użytkownika i na ich podstawie nie jest ustalana tożsamość użytkownika.
    Strona internetowa wydawcy (tu: należąca do CFA) może umieścić plik cookie w przeglądarce, jeśli przeglądarka to umożliwia. Co ważne, przeglądarka zezwala stronie internetowej na dostęp jedynie do plików cookies umieszczonych przez tę stronę, a nie do plików umieszczonych przez inne strony internetowe.
</p>


<h3>Dlaczego z tego korzystamy?</h3>

<p>Korzystamy z plików cookies i innych podobnych technologii w celach:</p>
<ul>
    <li>świadczenia usług;</li>
    <li>
        dostosowywania zawartości serwisów i aplikacji do preferencji Użytkownika oraz optymalizacji korzystania ze stron internetowych; np. pliki cookies pozwalają w szczególności rozpoznać urządzenie Użytkownika i odpowiednio wyświetlić stronę internetową dostosowaną do jego indywidualnych potrzeb;
    </li>
    <li>
        tworzenia statystyk, które pomagają zrozumieć, w jaki sposób Użytkownicy korzystają ze serwisów i aplikacji, co umożliwia ulepszanie ich struktury i zawartości;
    </li>
    <li>
        utrzymania sesji Użytkownika (po zalogowaniu), dzięki czemu Użytkownik nie musi na każdej podstronie internetowej danego serwisu i aplikacji ponownie wpisywać loginu i hasła;
    </li>
    <li>
        prezentacji reklam, m.in. w sposób uwzględniający zainteresowania Użytkownika czy jego miejsce zamieszkania (indywidualizowanie przekazu reklamowego) i z gwarancją wyłączenia możliwości wielokrotnego prezentowania Użytkownikowi tej samej reklamy;
    </li>
    <li>
        realizacji ankiet - w szczególności by uniknąć wielokrotnej prezentacji tej samej ankiety temu samemu Odbiorcy oraz by prezentować ankiety w sposób uwzględniający zainteresowania odbiorców.
    </li>
</ul>

<h3>Rodzaje plików cookies, które wykorzystujemy</h3>

<p>
    Z uwagi na czas życia cookies i innych podobnych technologii, stosujemy dwa zasadnicze rodzaje tych plików:
</p>
<ul>
    <li>sesyjne - pliki tymczasowe przechowywane w urządzeniu końcowym Użytkownika do czasu wylogowania, opuszczenia strony internetowej i aplikacji lub wyłączenia oprogramowania (przeglądarki internetowej);</li>
    <li>stałe - przechowywane w urządzeniu końcowym Użytkownika przez czas określony w parametrach plików cookies lub do czasu ich usunięcia przez Użytkownika.</li>
</ul>

<p>Ze względu na cel, jakiemu służą pliki cookies i inne podobne technologie, stosujemy ich następujące rodzaje:</p>
<ul>
    <li>niezbędne do działania usługi i aplikacji- umożliwiające korzystanie z naszych usług, np. uwierzytelniające pliki cookies wykorzystywane do usług wymagających uwierzytelniania;</li>
    <li>pliki służące do zapewnienia bezpieczeństwa, np. wykorzystywane do wykrywania nadużyć w zakresie uwierzytelniania
        wydajnościowe - umożliwiające zbieranie informacji o sposobie korzystania ze stron internetowych i aplikacji;</li>
    <li>funkcjonalne - umożliwiające "zapamiętanie" wybranych przez Użytkownika ustawień i personalizację interfejsu Użytkownika, np. w zakresie wybranego języka lub regionu, z którego pochodzi Użytkownik, rozmiaru czcionki, wyglądu strony internetowej i aplikacji itp.;</li>
    <li>reklamowe - umożliwiające dostarczanie Użytkownikom treści reklamowych bardziej dostosowanych do ich zainteresowań;
        statystyczne - służące do zliczana statystyk dotyczących stron internetowych i aplikacji.</li>
</ul>

<h3>Inne technologie</h3>

<p>Dla zapewnienia wygody użytkowania naszych serwisów używamy technologii Local Storage Object (LSO), zbliżonej zasadą działania do plików cookies, mającej jednak nieco inne właściwości.</p>
<p>LSO to wydzielona część pamięci przeglądarki, służąca do przechowywania danych zapisywanych przez serwisy. Dostęp do niej może uzyskać tylko strona internetowa działający w tej samej witrynie, z której dane zostały zapisane, jednak inaczej niż w przypadku cookies, nie są one wysyłane przez przeglądarkę przy każdym odwołaniu do serwera. Dane w Local Storage Object są długotrwale przechowywane przez przeglądarkę i nie są usuwane po zamknięciu przeglądarki ani nie mają określonego czasu ważności.</p>
<p>Podkreślamy, że używamy tej technologii wyłącznie dla wygody naszych użytkowników i w celu świadczenia im usług na jak najwyższym poziomie.</p>

<h3>Zarządzanie ustawieniami przeglądarki</h3>

<p>W wielu przypadkach oprogramowanie służące do przeglądania stron internetowych (przeglądarka internetowa) domyślnie dopuszcza przechowywanie informacji w formie plików cookies i innych podobnych technologii w urządzeniu końcowym Użytkownika. Użytkownik może jednak w każdym czasie dokonać zmiany tych ustawień. Niedokonanie zmian oznacza, że w/w informacje mogą być zamieszczane i przechowywane w jego urządzeniu końcowym, a tym samym że będziemy przechowywać informacje w urządzeniu końcowym użytkownika i uzyskiwać dostęp do tych informacji.</p>
<p>Z poziomu przeglądarki internetowej, z której Użytkownik korzysta możliwe jest np. samodzielne zarządzanie plikami cookies. W najpopularniejszych przeglądarkach istnieje m.in. możliwość:</p>
<ul>
    <li>
        zaakceptowania obsługi cookies, co pozwala Użytkownikowi na pełne korzystanie z opcji oferowanych przez witryny internetowe;
        zarządzania plikami cookies na poziomie pojedynczych, wybranych przez Użytkownika witryn;
    </li>
    <li>określania ustawień dla różnych typów plików cookie, na przykład akceptowania plików stałych jako sesyjnych itp.;
        blokowania lub usuwania cookies.</li>
</ul>

<h3>Informacje dodatkowe</h3>

<p>Dodatkowe informacje na temat plików cookies i innych technologii można znaleźć m.in. pod adresem wszystkoociasteczkach.pl, youronlinechoices.com lub w sekcji Pomoc w menu przeglądarki internetowej.</p>

<h3>Niepożądane treści</h3>

<p>W serwisach CFA nie ma miejsca na treści, które są sprzeczne z prawem polskim i międzynarodowym, nawołują do nienawiści rasowej, wyznaniowej czy etnicznej. Nie ma tu pornografii, materiałów nagannych moralnie, treści uważanych powszechnie za nieetyczne czy obelżywe.</p>