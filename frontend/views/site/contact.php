<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title .= 'Kontakt';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <div class="contact-box">
        <div class="padded-multiline">
            <h1 class="gradient-blue-heading">Masz ciekawego newsa lub chcesz się<br>skontaktować? Wypełnij formularz.</h1>
        </div>

        <?php if (Yii::$app->session->hasFlash('sent')): if (Yii::$app->session->hasFlash('sent') == true) : ?>
            <style>p.alert {margin-top: 20px;}</style>
            <p class="alert alert-success">Dziękujemy za kontakt z nami. Odpowiemy tak szybko, jak to możliwe</p>
        <?php elseif (Yii::$app->session->hasFlash('sent') == false) : ?>
            <p class="alert alert-danger">Wysyłanie wiadomości nie powiodło się.</p>
        <?php endif; endif; ?>

        <h2>Wybierz cel kontaktu</h2>
        <input type="checkbox" id="news">
        <label for="news" class="contact-purpose">mam newsa</label>
        <input type="checkbox" id="other">
        <label for="other" class="contact-purpose">inna sprawa</label>

        <h2 style="padding-bottom: 0">Wypełnij formularz</h2>
        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

            <?= $form->field($model, 'name')->textInput(['autofocus' => true])->input('text', ['placeholder' => 'Imię i nazwisko']) ?>
            <?= $form->field($model, 'email')->input('email', ['placeholder' => 'E-mail']); ?>
            <?= $form->field($model, 'subject')->input('text', ['placeholder' => 'Temat']) ?>
            <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className()) ?>

            <div class="form-group">
                <?= Html::submitButton('Wyślij', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>

        <div class="contact-right">
            <div class="address">
                <p class="header" style="text-transform: none; text-align: center; margin: 0; padding: 0;">
                    GameLoad
                </p>
            </div>
            <div class="email">
                <p>
                    Możesz również skontaktować się przez mail:
                </p>
                <p class="email-bigger">
                    kontakt@gameload.pl
                </p>
            </div>
        </div>
    </div>
</div>
