<?php

/* @var $this yii\web\View */
use yii\web\View;
use frontend\components\NewsTileWidget;

$this->title = Yii::$app->name . ' · Strona główna';
\frontend\assets\SlickAsset::register($this);

$fetchedNewsIdsCommas = implode(',', $fetchedNewsIds);

$js = <<<JS

var fetched = '$fetchedNewsIdsCommas'
var loading = false;

$('.slider').slick({
    'draggable': false,
    'arrows': false,
    'dots': true,
    'appendDots': $('.slider'),
    'autoplay': true,
    'slide': 'div'
});

$('#load-more').click(function () {
    if (loading) {
        return;
    }
    loading = true;
    $('#load-more').addClass('loading');

    $.ajax({
        type: 'get',
        url: '/site/more/' + fetched + '/6',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
        },
        success: function (r) {
            $('.more-news.row').prepend(r.data);
            fetched = r.fetchedNewsIds;
            console.log(r);
        },
        complete: function () {
            loading = false;
            $('#load-more').removeClass('loading');
        },
    });
});

JS;
$this->registerJs($js, \yii\web\View::POS_READY);

?>

<?php // slider ?>
<div class="row">
    <div class="slider col-xs-12">
        <?php foreach ($slider as $s): ?>
            <div class="news-500 news slide">
                <a class="news-link" href="<?= \yii\helpers\Url::to(['news/view', 'id' => $s->id, 'slug' => $s->slug]) ?>">
                    <div class="under-link-wrap">
                        <div class="news-cats">
                            <?php foreach ($s->categories as $c): ?>

                                <span<?= $c->color != '' ? ' style="color: ' . $c->color . '; border-color: ' . $c->color . ';"' : '' ?>><?= $c->name ?></span>
                            <?php endforeach; ?>
                        </div>
                        <div class="date">
                            <p><?= $s->date; ?></p>
                        </div>
                        <h3 class="title"><?= $s->title ?></h3>
                        <div class="news-img-wrapper"><div class="news-img" style="background-image: url('<?= $s->img ?>')"></div></div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php // section 1 ?>
<div class="row">
    <div class="col-xs-12 col-md-8">
        <div class="row">
            <?php foreach ($underSlider as $s) echo NewsTileWidget::widget(['model' => $s]); ?>
        </div>
    </div>
    <div class="col-xs-12 col-md-4 week">
        <h2>Najgorętsze artykuły tygodnia</h2>
        <div class="separator-week"></div>
        <ul>
            <?php foreach ($weekTop as $s): ?>
                <li>
                    <a href="<?= \yii\helpers\Url::to(['news/view', 'id' => $s->id, 'slug' => $s->slug]) ?>">
                        <div class="img" style="background-image: url('<?= $s->img ?>')"></div>
                        <h3><?= $s->title ?></h3>
                        <div class="clearfix"></div>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<?php // section 2 ?>
<div class="row">
    <div class="fb-like-section section-v-gradient-header">
        <a href="https://www.facebook.com/GameLoadPL">
            <div class="gradient">
                <img class="gradient-icon" src="<?= Yii::getAlias('@web/img/fb_icon_white.svg'); ?>">
            </div>
            <div class="padded-multiline">
                <h2>Chcesz być na bieżąco?<br>Polub nas na FB!</h2>
            </div>
        </a>
        <div class="clearfix"></div>
    </div>


    <div class="col-xs-12 col-md-8">
        <div class="row">
            <?php foreach ($nextToEvents as $s) echo NewsTileWidget::widget(['model' => $s]); ?>
        </div>
    </div>
    <div class="col-xs-12 col-md-4 event-wrapper">
        <div class="event-gradient"></div>
        <div class="header">nadchodzące wydarzenia tematyczne</div>
        <div class="row">
            <?php $i = 0; foreach ($events as $s): ?>
            <div class="news col-xs-12 <?= $i == 0 ? 'first' : 'second'; ?>">
                <a class="news-link" href="<?= \yii\helpers\Url::to(['event/view', 'id' => $s->id, 'slug' => $s->slug]) ?>">
                    <div class="under-link-wrap">
                        <div class="date-and-title">
                            <div class="date"><?= $s->date_from ?> <?= ($s->date_to != null) ? ' - ' . $s->date_to : ''; ?></div>
                            <h3 class="title gradient-blue-heading"><?= $s->name ?></h3>
                        </div>
                        <div class="news-img-wrapper"><div class="news-img" style="background-image: url('<?= $s->img ?>')"></div></div>
                    </div>
                </a>
            </div>
            <?php $i++; endforeach; ?>

            <a class="events-more" href="<?= \yii\helpers\Url::to(['event/list']); ?>">Zobacz więcej</a>
        </div>
    </div>
</div>

<div class="more-news row">
    <div class="row" style="margin-top: 20px;">
        <button class="big-button" id="load-more">Załaduj więcej</button>
    </div>
</div>