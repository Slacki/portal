<?php

/* @var $this yii\web\View */
use yii\web\View;

$this->title = $model->name . ' · GameLoad';

$this->registerJs("
$('.gallery').slick({
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    lazyLoad: 'progressive',
    variableWidth: true,
    focusOnSelect: true,
    arrows: false,
    dots: false, 
    responsive: [
        {
            breakpoint: 768,
            settings: {
                centerMode: false,
            }
        }
    ]
});", View::POS_READY);

$this->render('../layouts/facebookSdk.php');

?>
<div id="fb-root"></div>

<div class="news-head event-head">
    <div class="news-head-img" style="background-image: url('<?= $model->img ?>');">
        <div class="news-right">
            <div class="date">
                <?= $model->date_from ?><?= ($model->date_to != null) ? ' <br>&mdash;<br> ' . $model->date_to : ''; ?>
            </div>
            <div class="place"><?= $model->place ?></div>
        </div>
    </div>
</div>

<div class="news-content">
    <h1 class="h1-event"><?= $model->name ?></h1>
    <div class="news-h-separator"></div>

    <?= $this->render('../layouts/contentWithWidgets.php', ['model' => $model]) ?>

</div>
<div class="clearfix"></div>

<?php if ($gallery != null) : ?>
    <div class="gallery">
        <?php foreach ($gallery as $g) : ?>
            <img data-lazy="<?= $g->img; ?>">
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<?= $this->render('../layouts/facebookSocial.php') ?>
