<?php

/* @var $this yii\web\View */
use yii\web\View;

$this->title = Yii::$app->name . ' · Nadchodzące wydarzenia';
\frontend\assets\SlickAsset::register($this);
?>

<div class="padded-multiline">
    <h1 class="gradient-blue-heading">Nadchodzące wydarzenia</h1>
</div>

<?php if (count($models) <= 0): ?>
    <p class="no-content">
        Brak wyników.
    </p>
<?php endif; ?>

<section class="row section-category event-wrapper event-wrapper-list">
    <?php foreach ($models as $k => $m) : ?>
        <div class="row">
            <h4><?= $k ?></h4>
            <?php foreach ($m as $s) : ?>
                <div class="news col-xs-12 col-sm-6">
                    <a class="news-link" href="<?= \yii\helpers\Url::to(['event/view', 'id' => $s->id, 'slug' => $s->slug]) ?>">
                        <div class="under-link-wrap">
                            <div class="date-and-title">
                                <div class="date"><?= $s->date_from ?> <?= ($s->date_to != null) ? ' - ' . $s->date_to : ''; ?></div>
                                <h3 class="title gradient-blue-heading"><?= $s->name ?></h3>
                            </div>
                            <div class="news-img-wrapper"><div class="news-img" style="background-image: url(<?= $s->img ?>)"></div></div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</section>