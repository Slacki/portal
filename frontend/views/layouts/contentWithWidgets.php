<?php

$text = isset($model->text) ? $model->text : $model->description;

$patternProduct = '@\<.*\>\[product_(?<id>\d+)\]\<.*\>@';
$patternPoll = '@\<.*\>\[poll_(?<id>\d+)\]\<.*\>@';

preg_match($patternPoll, $text, $matchesPoll);
preg_match($patternProduct, $text, $matchesProduct);

$widgetPoll = null;
if (isset($matchesPoll['id'])) {
    ob_start();
    echo \frontend\components\PollWidget::widget(['id' => $matchesPoll['id']]);
    $widgetPoll = ob_get_clean();
}
$widgetProduct = null;
if (isset($matchesProduct['id'])) {
    ob_start();
    echo \frontend\components\ProductWidget::widget(['id' => $matchesProduct['id']]);
    $widgetProduct = ob_get_clean();
}

if ($widgetPoll != null) {
    $text = preg_replace($patternPoll, $widgetPoll, $text);
}
if ($widgetProduct != null) {
    $text = preg_replace($patternProduct, $widgetProduct, $text);
}

echo $text;