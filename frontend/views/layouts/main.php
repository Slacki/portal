<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

$categories = $this->params['categories'];

$cookiesText = 'Ten serwis używa plików cookies do prawidłowego działania. Jeżeli się z tym nie zgadzasz, zmień ustawienia swojej przeglądarki internetowej. Więcej informacji w naszej <a href="' . \yii\helpers\Url::to('site/privacy-policy') . '">polityce prywatności</a>.';

$this->registerJs("
    if ($(document).height() <= $(window).height()) {
        $('footer').addClass(\"navbar-fixed-bottom\");
    } else {
        $('footer').removeClass(\"navbar-fixed-bottom\");
    }
    
    $('.event-gradient').width($(window).width());
    $(window).on('resize', function (e) {
        $('.event-gradient').width($(window).width());
    });
    
    $('.head-search').click(function () {
        $('.sb-search-input').animate({width: 'toggle'});
        $('.head-search').toggleClass('active');
    });
    
    // cookies
    jQuery.fn.cookiesEU({
		text: '$cookiesText',
		close: 'Zamknij',
		position: 'top',
		auto_accept: false,
	});
", \yii\web\View::POS_READY);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="GameLoad to serwis dla fanów nowinek technologicznych oraz najświeższych gier elektronicznych i analogowych. Recenzje, testy, felietony oraz publicystyka. To wszystko znajdziecie na naszej stronie!">
    <meta name="keywords" content="gameload, newsy, gry komputerowe, konsole, technologia komputerowa, najświeższe newsy o grach, premiery gier, najciekawsze gry, gry">

    <?php if ($this->params['news']): ?>
        <meta property="og:url" content="<?= Yii::$app->request->absoluteUrl ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="<?= $this->params['newsTitle']; ?>" />
        <meta property="og:description" content="<?= $this->params['newsExcerpt'] ?>" />
        <meta property="og:image" content="<?= $this->params['newsImg'] ?>" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="<?= $this->params['newsTitle']; ?>" />
        <meta name="twitter:description" content="<?= $this->params['newsExcerpt'] ?>" />
        <meta name="twitter:image" content="<?= $this->params['newsImg'] ?>" />
    <?php endif; ?>

    <?php $fav = \yii\helpers\Url::base(true) . '/img/fav' ?>

    <link rel="apple-touch-icon" sizes="57x57" href="<?= $fav ?>/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= $fav ?>/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= $fav ?>/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= $fav ?>/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= $fav ?>/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= $fav ?>/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= $fav ?>/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= $fav ?>/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= $fav ?>/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= $fav ?>/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $fav ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= $fav ?>/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $fav ?>/favicon-16x16.png">
    <link rel="manifest" href="<?= $fav ?>/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= $fav ?>/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta itemprop="image" content="<?= $fav ?>/favicon-96x96.png">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="container">
    <header>
        <div id="header-gradient"></div>
        <div id="logo-nav">
            <a href="<?= \yii\helpers\Url::to(['/']); ?>">
                <img id="logo" src="<?= Yii::getAlias('@web/img/logotype_color_dark.svg'); ?>">
            </a>
            <nav>
                <ul>
                    <li class="text-hide"><a href="<?= \yii\helpers\Url::to(['comic/list']) ?>">komiksy</a></li>
                    <li class="text-hide"><a href="<?= \yii\helpers\Url::to(['site/contact']) ?>">kontakt</a></li>

                    <li class="">
                        <a href="https://www.facebook.com/GameLoadPL">
                            <div class="head-icon head-fb"></div>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/GameLoadPL">
                            <div class="head-icon head-ig"></div>
                        </a>
                    </li>
                    <li>
                        <form id="head-form" method="post" action="<?= \yii\helpers\Url::to(['news/search']); ?>">
                            <input class="sb-search-input" placeholder="fraza..." value="" name="search" id="search">
                            <input type="hidden" name="_csrf-frontend" value="<?=Yii::$app->request->getCsrfToken()?>" />
                            <div class="head-icon head-search"></div>
                        </form>
                    </li>
                </ul>
            </nav>
            <div id="categories">
                <ul>
                    <?php foreach ($categories as $c) : ?>
                        <li><?= Html::a($c->name, ['category/view', 'id' => $c->id, 'slug' => $c->slug]); ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </header>

    <div id="categories-mobile"></div>

    <?= $content; ?>
</div>

<footer>
    <div class="footer-clip"></div>
    <div class="container">
        <div class="col-sm-3 col-sm-offset-1">
            <a class="a-no-font" href="<?= \yii\helpers\Url::to(['/']); ?>">
                <img class="img-responsive logo-footer" src="<?= Yii::getAlias('@web/img/logotype_grey_footer.svg'); ?>">
            </a>
        </div>
        <div class="col-sm-7 col-sm-offset-1">
            <div class="row">
                <div class="col-sm-8">
                    <h2 class="footer-map">Mapa strony</h2>
                    <ul class="cats-footer">
                        <?php foreach ($categories as $c) : ?>
                            <li><?= Html::a($c->name, ['category/view', 'id' => $c->id, 'slug' => $c->slug]); ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <a href="<?= \yii\helpers\Url::to(['site/contact']) ?>" class="footer-a-important pull-top">kontakt</a>
                    <a href="<?= \yii\helpers\Url::to(['site/advertisement']) ?>" class="footer-a-important">reklama</a>
                    <div class="social-footer-wrapper">
                        <a class="a-no-font" href="https://www.facebook.com/GameLoadPL">
                            <img class="social" src="<?= Yii::getAlias('@web/img/footer_social_fb.svg'); ?>">
                        </a>
                        <a class="a-no-font" href="https://www.instagram.com/GameLoadPL">
                            <img class="social" src="<?= Yii::getAlias('@web/img/footer_social_insta.svg'); ?>">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <a class="a-small footer-a-important" href="<?= \yii\helpers\Url::to(['site/privacy-policy']) ?>">polityka prywatności</a>
        <p>Copyright <?= date('Y'); ?> GameLoad.pl &mdash; wszelkie prawa zastrzeżone</p>
    </div>
    <div class="orange-end-gradient"></div>
</footer>

<?php 
if (YII_ENV_PROD) {
    include_once(Yii::getAlias('@frontend/config/tracking.php'));
}
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>