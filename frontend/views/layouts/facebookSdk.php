<?php

use yii\web\View;

$this->registerJs("
    window.fbAsyncInit = function() {
        FB.init({
        appId            : '1331039763585868',
        autoLogAppEvents : true,
        xfbml            : true,
        version          : 'v3.1'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src ='https://connect.facebook.net/pl_PL/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));", View::POS_BEGIN);

?>