<?php

namespace backend\controllers;

use common\models\PollQuestion;
use common\models\User;
use Yii;
use common\models\Poll;
use common\models\PollSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PoolController implements the CRUD actions for Pool model.
 */
class PollController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Pool models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PollSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,
            \Yii::$app->user->identity->level > User::LEVEL_REDACTOR ? true : false);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pool model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pool model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Poll();
        $model->user_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            foreach (Yii::$app->request->post('Questions') as $q) {
                $pk = new PollQuestion();
                $pk->votes = 0;
                $pk->poll_id = $model->id;
                $pk->text = $q['text'];
                $pk->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pool model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (\Yii::$app->user->identity->level == User::LEVEL_REDACTOR) {
            if ($model->user_id != \Yii::$app->user->id) {
                Yii::info(
                    'Access violation (not author of poll): User ['
                    . Yii::$app->user->identity->username . '] at ' . __METHOD__
                    , 'security'
                );
                throw new ForbiddenHttpException('You are not allowed to perform this action');
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            foreach (Yii::$app->request->post('Questions') as $id => $q) {
                $pk = PollQuestion::findOne(['id' => $id]);
                $pk->text = $q['text'];
                $pk->save();
            }
            Yii::info('[poll][update][' . $model->id . '][' . $model->question . ']', 'userBehaviour');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'questions' => PollQuestion::find()->where(['poll_id' => $model->id])->limit(5)->all(),
            ]);
        }
    }

    /**
     * Deletes an existing Pool model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (\Yii::$app->user->identity->level == User::LEVEL_REDACTOR) {
            if ($model->user_id != \Yii::$app->user->id) {
                Yii::info(
                    'Access violation (not author of poll): User ['
                    . Yii::$app->user->identity->username . '] at ' . __METHOD__
                    , 'security'
                );
                throw new ForbiddenHttpException('You are not allowed to perform this action');
            }
        }

        Yii::info('[poll][delete][' . $model->id . '][' . $model->question . ']', 'userBehaviour');

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pool model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Poll the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Poll::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
