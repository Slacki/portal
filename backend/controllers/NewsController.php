<?php

namespace backend\controllers;

use common\models\Category;
use common\models\NewsWithCategories;
use common\models\User;
use Yii;
use common\models\News;
use common\models\NewsSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(
            Yii::$app->request->queryParams,
            Yii::$app->user->identity->level != User::LEVEL_REDACTOR
        );

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param string $id
     * @param string $user_id
     * @param string $category_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsWithCategories();

        if ($model->load(Yii::$app->request->post())) {
            // news unverified when posted by redactor
            if (Yii::$app->user->identity->level === User::LEVEL_REDACTOR) {
                $model->status = News::STATUS_NOT_VERIFIED;
            }
            if ($model->user_id == null) {
                $model->user_id = Yii::$app->user->id;
            }
            if ($model->save()) {
                $model->saveCategories();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'categories' => Category::getAvailableCategories(),
            'users' => User::getAllUsers(),
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $user_id
     * @param string $category_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = NewsWithCategories::findOne($id);
        $model->loadCategories();

        if (Yii::$app->user->identity->level == User::LEVEL_REDACTOR) {
            if ($model->status == News::STATUS_VERIFIED) {
                Yii::info(
                    'Access violation (news already accepted): User ['
                    . Yii::$app->user->identity->username . '] at ' . __METHOD__
                    , 'security'
                );
                throw new ForbiddenHttpException('You are not allowed to perform this action');
            }
            if ($model->user_id != Yii::$app->user->id) {
                Yii::info(
                    'Access violation (not author of news): User ['
                    . Yii::$app->user->identity->username . '] at ' . __METHOD__
                    , 'security'
                );
                throw new ForbiddenHttpException('You are not allowed to perform this action');
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->saveCategories();
            Yii::info('[news][update][' . $model->id . '][' . $model->title . ']', 'userBehaviour');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'categories' => Category::getAvailableCategories(),
                'users' => User::getAllUsers()
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $user_id
     * @param string $category_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->identity->level == User::LEVEL_REDACTOR) {
            if ($model->status == News::STATUS_VERIFIED) {
                Yii::info(
                    'Access violation (news already accepted): User ['
                    . Yii::$app->user->identity->username . '] at ' . __METHOD__
                    , 'security'
                );
                throw new ForbiddenHttpException('You are not allowed to perform this action');
            }
            if ($model->user_id != Yii::$app->user->id) {
                Yii::info(
                    'Access violation (not author of news): User ['
                    . Yii::$app->user->identity->username . '] at ' . __METHOD__
                    , 'security'
                );
                throw new ForbiddenHttpException('You are not allowed to perform this action');
            }
        }

        Yii::info('[news][delete][' . $model->id . '][' . $model->title . ']', 'userBehaviour');

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param string $user_id
     * @param string $category_id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
