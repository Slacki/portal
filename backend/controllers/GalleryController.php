<?php

namespace backend\controllers;

use common\models\Gallery;
use common\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class GalleryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionList($eventId = null, $newsId = null)
    {
        if ($eventId == null && $newsId == null) {
            throw new BadRequestHttpException();
        }

        $models = $eventId == null ?
            Gallery::find()->where(['parent_id' => $newsId])->andWhere(['parent_type' => Gallery::PARENT_NEWS])->all() :
            Gallery::find()->where(['parent_id' => $eventId])->andWhere(['parent_type' => Gallery::PARENT_EVENT])->all();

        return $this->render('list', [
            'models' => $models,
            'newsId' => $newsId,
            'eventId' => $eventId
        ]);
    }

    public function actionCreate($eventId = null, $newsId = null)
    {
        if ($eventId == null && $newsId == null) {
            throw new BadRequestHttpException();
        }

        $model = new Gallery();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;

            if ($eventId == null) {
                $model->parent_id = $newsId;
                $model->parent_type = Gallery::PARENT_NEWS;
            } else {
                $model->parent_id = null;
                $model->event_id = Gallery::PARENT_EVENT;
            }

            if ($model->save()) {
                Yii::info('[gallery][create][' . $model->id . '][parent-id:' . $model->parent_id . ']', 'userBehaviour');
                return $this->redirect(['gallery/list', 'newsId' => $newsId, 'eventId' => $eventId]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'newsId' => $newsId,
            'eventId' => $eventId
        ]);
    }

    public function actionDelete($id)
    {
        $model = Gallery::findOne($id);
        if ($model == null) {
            throw new BadRequestHttpException();
        }
        $returnId = $model->parent_id;
        $returnType = $model->parent_type;

        $returnParam = [];
        if ($returnType == Gallery::PARENT_NEWS) {
            $returnParam['newsId'] = $returnId;
        } else {
            $returnParam['eventId'] = $returnId;
        }

        if (Yii::$app->user->identity->level == User::LEVEL_REDACTOR) {
            if ($model->user_id != Yii::$app->user->id) {
                Yii::info('Access violation: User [' . Yii::$app->user->identity->username . '] at ' . __METHOD__, 'security');
                throw new ForbiddenHttpException('You are not allowed to perform this action.');
            }
        }

        Yii::info('[gallery][delete][' . $model->id . '][parent-id:' . $model->parent_id . ']', 'userBehaviour');

        $model->delete();

        return $this->redirect(ArrayHelper::merge(['gallery/list'], $returnParam));
    }
}
