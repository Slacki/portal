<?php

namespace backend\controllers;

use common\models\News;
use common\models\User;
use Yii;
use common\models\NewsAttach;
use common\models\NewsAttachSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NewsAttachController implements the CRUD actions for NewsAttach model.
 */
class NewsAttachController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($role, $action) {
                            $res = Yii::$app->user->identity->level >= User::LEVEL_EDITOR;
                            if (!$res) {
                                Yii::info(
                                    'Access violation: User [' . Yii::$app->user->identity->username . '] at ' . __CLASS__
                                    , 'security'
                                );
                            }

                            return $res;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsAttach models.
     * @return mixed
     */
    public function actionIndex()
    {
        $data = [];
        $data['slider'] = NewsAttach::find()->where(['attachment' => NewsAttach::POS_SLIDER])->all();

        return $this->render('index', $data);
    }

    /**
     * Displays a single NewsAttach model.
     * @param string $id
     * @param string $news_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NewsAttach model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsAttach();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::info('[news-attach][create][' . $model->id . ']', 'userBehaviour');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'news' => News::getAllNews(),
            ]);
        }
    }

    /**
     * Updates an existing NewsAttach model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $news_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::info('[news-attach][update][' . $model->id . ']', 'userBehaviour');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'news' => News::getAllNews(),
            ]);
        }
    }

    /**
     * Deletes an existing NewsAttach model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @param string $news_id
     * @return mixed
     */
    public function actionDelete($id, $news_id)
    {
        $model = $this->findModel($id, $news_id);
        Yii::info('[news-attach][delete][' . $model->id . ']', 'userBehaviour');
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NewsAttach model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @param string $news_id
     * @return NewsAttach the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsAttach::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
