<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'first_name')->textInput() ?>
    <?= $form->field($model, 'last_name')->textInput() ?>
    <?= $form->field($model, 'nick')->textInput() ?>
    <?= $form->field($model, 'about')->textarea() ?>

    <?= $form->field($model, 'password')->textInput() ?>
    <p class="hint">
        Jeżeli edytujesz użytkownika i wpiszesz tutaj hasło, to zostanie ono nadpisane. Nie wpisuj nic, jeżeli nie chcesz zmieniać hasła.<br>
        Przy nowym użytkowniku podaj dobre hasło. Hasło "penismeski" jest słabe, twórzcie dobre hasła, serio.
    </p>

    <?= $form->field($model, 'level')->widget(\kartik\select2\Select2::classname(), [
        'name' => 'level',
        'value' => '',
        'data' => [
            \common\models\User::LEVEL_ADMIN => 'Administrator (10)',
            \common\models\User::LEVEL_MOD => 'Moderator (9)',
            \common\models\User::LEVEL_EDITOR => 'Edytor (8)',
            \common\models\User::LEVEL_REDACTOR => 'Redaktor (1)',
        ],
        'options' => ['placeholder' => 'Wybierz uprawnienia']
    ]); ?>

    <?= $form->field($model, 'status')->widget(\kartik\select2\Select2::classname(), [
        'name' => 'status',
        'value' => '',
        'data' => [
            \common\models\User::STATUS_ACTIVE => 'Aktywny (10)',
            \common\models\User::STATUS_DELETED => 'Usunięty (0)',
        ],
        'options' => ['placeholder' => 'Wybierz uprawnienia']
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
