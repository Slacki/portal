<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="alert alert-info">
        Zamiast usuwać użytkownika edytuj jego status i ustaw na <strong>usunięty!</strong><br>
        Jeśli go usuniesz to znikną informacje o autorze w newsie, a tego nie chcemy.
    </p>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'level',
            'status',
            'email:ntext',
            'username',
            // 'password_hash',
            // 'auth_key',
            // 'first_name',
            // 'last_name',
            // 'nick',
            // 'about',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
