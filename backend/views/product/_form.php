<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("$('.show-roxy').click(function (e) {
    e.preventDefault();
    $('#roxy-iframe').toggle();
});")
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <p class="alert alert-info">
        Kolejne zalety/wady oddzielamy enterem (nowa linia). Przykład:<br><br>
        zaleta1<br>
        zaleta2<br>
        bardzo długa zaleta
    </p>

    <?= $form->field($model, 'pros')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cons')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'stars')->dropDownList([
            '0' => '0',
            '1' => '0.5',
            '2' => '1',
            '3' => '1.5',
            '4' => '2',
            '5' => '2.5',
            '6' => '3',
            '7' => '3.5',
            '8' => '4',
            '9' => '4.5',
            '10' => '5',
    ]) ?>

    <?= $form->field($model, 'img')->textInput(['id' => 'img-path']); ?>
    <button class="show-roxy btn btn-default btn-sm" style="margin-bottom: 10px;">Manager</button>

    <iframe id="roxy-iframe" style="display: none; width: 1000px; height: 600px;" src="<?= \yii\helpers\Url::to([
        '/roxymce/default',
        'type'   => 'image',
        'input'  => 'img-path',
        'dialog' => 'modal',
    ]) ?>"></iframe>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
