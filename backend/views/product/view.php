<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pros:ntext',
            'cons:ntext',
            [
                'label' => 'Gwiazdki',
                'value' => $model->stars / 2
            ]
        ],
    ]) ?>

    <p class="alert alert-info">
        Wklej ten kod w treści newsa, w miejscu, gdzie chcesz umieścić ocenę produktu. <br>
        <strong>Nie modyfikować kodu!</strong>
    </p>

    <pre>[product_<?= $model->id; ?>]</pre>

</div>
