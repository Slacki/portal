<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss("
    .nav-visible {
        background: #95de56;
        padding: 5px 20px;
        color: black;
        margin: 0 20px;
        text-decoration: none;
    }
    
    .nav-invisible {
        background: #de5858;
    }
    
    .item {
        margin: 20px 0;
    }
");
?>

<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="alert alert-danger">
        Usunięcie kategorii usunie <strong>wszystkie</strong> powiązania kategorii z newsami!!!
    </p>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(
                Html::encode($model->name)
                . ($model->nav == 1 ? '<span class="nav-visible"> widoczne w menu</span>' : '<span class="nav-visible nav-invisible"> nie widoczne w menu</span>')
                , ['view', 'id' => $model->id]
            );
        },
    ]) ?>
<?php Pjax::end(); ?></div>
