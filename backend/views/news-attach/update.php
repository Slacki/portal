<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NewsAttach */

$this->title = 'Update News Attach: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'News Attaches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'news_id' => $model->news_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="news-attach-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'news' => $news,
    ]) ?>

</div>
