<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsAttach */

$this->title = 'Create News Attach';
$this->params['breadcrumbs'][] = ['label' => 'News Attaches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-attach-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'news' => $news
    ]) ?>

</div>
