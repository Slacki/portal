<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsAttachSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News Attaches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-attach-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p class="alert alert-info">
        Możesz tutaj przypiąć newsy na stałe w danych pozycjach. Będą miały pierwszeństwo przed tymi
        wyświetlanymi przez system.
    </p>

    <p><?= Html::a('Create News Attach', ['create'], ['class' => 'btn btn-success']) ?></p>

    <h2>Sekcje</h2>
    <h3>Slider</h3>
    <ul>
    <?php foreach ($slider as $d): ?>
        <li><a href="<?= \yii\helpers\Url::to(['news-attach/view', 'id' => $d->id]); ?>"><?= $d->news->title; ?></a></li>
    <?php endforeach; ?>
    </ul>
</div>
