<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\NewsAttach */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-attach-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'attachment')->widget(\kartik\select2\Select2::classname(), [
        'name' => 'attachment',
        'value' => '',
        'data' => [
                \common\models\NewsAttach::POS_SLIDER => 'Slider',
        ],
        'options' => ['placeholder' => 'Wybierz pozycję']
    ]); ?>

    <?= $form->field($model, 'news_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => $news,
        'language' => 'pl',
        'options' => ['placeholder' => 'Wybierz news'],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
