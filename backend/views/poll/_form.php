<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Poll */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("$('.show-roxy').click(function (e) {
    e.preventDefault();
    $('#roxy-iframe').toggle();
});")
?>

<div class="pool-form">

    <?php $form = ActiveForm::begin([
            'enableClientValidation' => false,
    ]); ?>

    <fieldset>
        <legend>Ankieta</legend>
        <?= $form->field($model, 'question')->textInput(); ?>
        <?= $form->field($model, 'img')->textInput(['id' => 'img-path']); ?>
        <button class="show-roxy btn btn-default btn-sm" style="margin-bottom: 10px;">Manager</button>

        <iframe id="roxy-iframe" style="display: none; width: 1000px; height: 600px;" src="<?= \yii\helpers\Url::to([
            '/roxymce/default',
            'type'   => 'image',
            'input'  => 'img-path',
            'dialog' => 'modal',
        ]) ?>"></iframe>
    </fieldset>

    <fieldset>
        <legend style="padding-top: 20px;">Pytania</legend>
        <p>Jeżeli chcesz mniej pytań, zostaw puste pola</p>

        <?php if ($model->isNewRecord):
            for ($i = 0; $i < 5; $i++): ?>
                <div class="form-group">
                    <?= Html::input('string', "Questions[$i][text]", null, ['class' => 'form-control']); ?>
                </div>
            <?php endfor;
        else:
            foreach ($questions as $q): ?>
                <div class="form-group">
                    <?= Html::input('string', "Questions[$q->id][text]", $q->text, ['class' => 'form-control']); ?>
                </div>
        <?php endforeach; endif; ?>
    </fieldset>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
