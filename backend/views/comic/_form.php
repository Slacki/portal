<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Comic */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("$('.show-roxy').click(function (e) {
    e.preventDefault();
    $('#roxy-iframe').toggle();
});")
?>

<div class="comic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'img')->textInput(['id' => 'img-path']); ?>
    <button class="show-roxy">Otwórz manager plików</button>

    <iframe id="roxy-iframe" style="display: none; width: 1000px; height: 600px;" src="<?= \yii\helpers\Url::to([
        '/roxymce/default',
        'type'   => 'image',
        'input'  => 'img-path',
        'dialog' => 'modal',
    ]) ?>"></iframe>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= \dosamigos\datepicker\DatePicker::widget([
        'model' => $model,
        'attribute' => 'date',
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy'
        ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
