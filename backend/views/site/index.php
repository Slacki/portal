<?php

/* @var $this yii\web\View */

$this->title = 'Centrum Dowodzenia';
?>
<div class="site-index">

    <h1>Centrum Dowodzenia</h1>

    <p class="alert alert-info">
        Ten serwis używa plików cookies w celach uwierzytelnienia użytkownika.
    </p>
    <p class="alert alert-warning">
        Adresy IP oraz wszystkie działania i próby działań są rejestrowane w celach kontrolnych.
    </p>
</div>
