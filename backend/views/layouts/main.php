<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Panel Administracyjny</title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Centrum dowodzenia',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Wyloguj (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    if (!Yii::$app->user->isGuest && Yii::$app->user->identity->level >= \common\models\User::LEVEL_REDACTOR) {
        $menuItems[] = ['label' => 'Newsy', 'url' => ['news/index']];
        $menuItems[] = ['label' => 'Wydarzenia', 'url' => ['event/index']];
        $menuItems[] = ['label' => 'Ankiety', 'url' => ['poll/index']];
        $menuItems[] = ['label' => 'Produkty', 'url' => ['product/index']];
        $menuItems[] = ['label' => 'Komiksy', 'url' => ['comic/index']];
    }
    if (!Yii::$app->user->isGuest && Yii::$app->user->identity->level >= \common\models\User::LEVEL_EDITOR) {
        $menuItems[] = ['label' => 'Przypnij newsy', 'url' => ['news-attach/index']];
    }
    if (!Yii::$app->user->isGuest && Yii::$app->user->identity->level >= \common\models\User::LEVEL_MOD) {
        $menuItems[] = ['label' => 'Kategorie', 'url' => ['category/index']];
    }
    if (!Yii::$app->user->isGuest && Yii::$app->user->identity->level == \common\models\User::LEVEL_ADMIN) {
        $menuItems[] = ['label' => 'Użytkownicy', 'url' => ['user/index']];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Creative Future Agency <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
