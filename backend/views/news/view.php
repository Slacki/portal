<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(
                'Realny podgląd',
                Yii::$app->params['frontendUrl'] . Yii::$app->urlManagerFrontend->createUrl(['news/view', 'id' => $model->id, 'slug' => $model->slug]),
                ['class' => 'btn btn-default']
        ); ?>
        <?= Html::a(
                'Galeria dla tego newsa',
                ['gallery/list', 'newsId' => $model->id],
                ['class' => 'btn btn-default']
            ); ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title:ntext',
            'text:ntext',
            'date',
            'views',
            'slug:ntext',
            'user_id',
        ],
    ]) ?>

</div>
