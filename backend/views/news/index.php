<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create News', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'views',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($data) {
                    $text = '';
                    switch ($data->status) {
                        case \common\models\News::STATUS_VERIFIED:
                            $text = 'zweryfikowany'; break;
                        case \common\models\News::STATUS_NOT_VERIFIED:
                            $text = 'nie zweryfikowany'; break;
                        case \common\models\News::STATUS_DRAFT:
                            $text = 'szkic'; break;
                        default:
                            $text = '';
                    }
                    return $text;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>