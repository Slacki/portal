<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("$('.show-roxy').click(function (e) {
    e.preventDefault();
    $('#roxy-iframe').toggle();
});")
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'img')->textInput(['id' => 'img-path']); ?>
    <button class="show-roxy btn btn-default btn-sm" style="margin-bottom: 10px;">Manager</button>

    <iframe id="roxy-iframe" style="display: none; width: 1000px; height: 600px;" src="<?= \yii\helpers\Url::to([
        '/roxymce/default',
        'type'   => 'image',
        'input'  => 'img-path',
        'dialog' => 'modal',
    ]) ?>"></iframe>
    <p class="hint">
        To jest główny obrazek newsa wyświetlany w miniaturce i jednocześnie w widoku newsa u góry.<br>
        Użyj managera aby wgrać obrazek, następnie wybierz go i kliknij select.<br>
        Nazwy plików i katalogów są widoczne w kodzie HTML strony, <strong style="color: red;">proszę nie robić burdelu.</strong>
    </p>

    <?php if (Yii::$app->user->identity->level > \common\models\User::LEVEL_REDACTOR) : ?>
    <?= $form->field($model, 'status')->widget(\kartik\select2\Select2::classname(), [
        'name' => 'status',
        'value' => '',
        'data' => [
            \common\models\News::STATUS_VERIFIED => 'Zweryfikowany / widoczny na stronie',
            \common\models\News::STATUS_DRAFT => 'Szkic / nie widoczny na stronie',
        ],
        'options' => ['placeholder' => 'Wybierz status']
    ]); ?>
    <?php endif; ?>

    <?= $form->field($model, 'title')->textInput() ?>
    <p class="hint">
        Tytuł powinien być zwięzły, mniej więcej 8-12 słów. Nie wrzucać tu jakichś udziwnień typu &hearts; czy innych uśmieszków
    </p>

    <?= \dosamigos\datepicker\DatePicker::widget([
        'model' => $model,
        'attribute' => 'date',
        'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy'
        ]
    ]);?>
    <p class="hint">
        Można wybrać datę jaką się chce, ale data newsa ma wpływ na jego wyświetlanie na stronie głównej, np. news z datą rok wstecz nie pojawi się w "hot", "top 10 tygodnia" etc.
    </p>

    <?php if (Yii::$app->user->identity->level == \common\models\User::LEVEL_ADMIN) : ?>
    <?= $form->field($model, 'user_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => $users,
        'language' => 'pl',
        'options' => ['placeholder' => 'Autor newsa'],
        'pluginOptions' => [
            'allowClear' => false,
        ],
    ]); ?>
    <?php endif; ?>

    <?= $form->field($model, 'category_ids')
        ->checkboxList($categories)->hint('dodaj kategorie dla tego newsa');
    ?>

    <?= \navatech\roxymce\widgets\RoxyMceWidget::widget([
        'model' => $model,
        'attribute' => 'text'
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
