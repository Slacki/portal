<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Add image';

$this->registerJs("$('.show-roxy').click(function (e) {
    e.preventDefault();
    $('#roxy-iframe').toggle();
});")

/* @var $form yii\widgets\ActiveForm */
?>
<div class="gallery-img-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'img')->textInput(['id' => 'img-path']); ?>
    <button class="show-roxy btn btn-default btn-sm" style="margin-bottom: 10px;">Manager</button>

    <iframe id="roxy-iframe" style="display: none; width: 1000px; height: 600px;" src="<?= \yii\helpers\Url::to([
        '/roxymce/default',
        'type'   => 'image',
        'input'  => 'img-path',
        'dialog' => 'modal',
    ]) ?>"></iframe>

    <br>
    <button class="btn btn-success">Create</button>

    <?php $form = ActiveForm::end() ?>

</div>
