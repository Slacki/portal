<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $m common\models\Gallery */

$this->title = 'Gallery';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="news-index">
    <h1>Gallery</h1>

    <p>
        <?= Html::a('Add image', ['create', 'eventId' => $eventId, 'newsId' => $newsId], ['class' => 'btn btn-success']) ?>
    </p>

    <?php foreach ($models as $m) : ?>
        <div class="gallery-list-row" style="margin: 20px 0">
            <img src="<?= $m->img ?>" style="width: 500px; margin: 20px;">
            <?= Html::a('Delete', ['delete', 'id' => $m->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    <?php endforeach; ?>
</div>