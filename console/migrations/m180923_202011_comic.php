<?php

use yii\db\Migration;

/**
 * Class m180923_202011_comic
 */
class m180923_202011_comic extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('comic', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'img' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'date' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('comic');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180923_202011_comic cannot be reverted.\n";

        return false;
    }
    */
}
