<?php

use yii\db\Migration;

/**
 * Class m180923_190900_initial
 */
class m180923_190900_initial extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'slug' => $this->string(255)->notNull(),
            'nav' => $this->integer(4)->notNull()->defaultValue(1),
            'color' => $this->string(255)->notNull(),
        ]);

        $this->createTable('event', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'date_from' => $this->integer(11)->notNull(),
            'date_to' => $this->integer(11)->notNull(),
            'place' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'img' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
        ]);

        $this->createTable('gallery', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer(11)->notNull(),
            'event_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'parent_id' => $this->integer(11)->notNull(),
            'parent_type' => $this->integer(11)->notNull(),
            'img' => $this->string()->notNull(),
        ]);
          
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'text' => $this->string()->notNull(),
            'status' => $this->integer()->defaultValue(null),
            'date' => $this->integer()->notNull(),
            'views' => $this->integer()->defaultValue(1),
            'slug' => $this->string(),
            'img' => $this->string()->notNull(),
        ]);
          
        $this->createTable('news_attach', [
            'id' => $this->primaryKey(),
            'attachment' => $this->integer()->notNull(),
            'news_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('news_has_category', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ]);
        
        $this->createTable('news_has_tag', [
            'news_id' => $this->integer()->notNull(),
            'news_user_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
        ]);
        
        $this->createTable('poll', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'question' => $this->string()->notNull(),
            'img' => $this->string()->notNull(),
        ]);
        
        $this->createTable('poll_question', [
            'id' => $this->primaryKey(),
            'poll_id' => $this->integer()->notNull(),
            'votes' => $this->string()->notNull(),
            'text' => $this->string()->notNull(),
        ]);
          
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'stars' => $this->integer()->notNull(),
            'pros' => $this->string()->notNull(),
            'cons' => $this->string()->notNull(),
            'img' => $this->string()->notNull(),
        ]);
          
        $this->createTable('session', [
            'id' => $this->char(40)->notNull(),
            'expire' => $this->integer()->defaultValue(null),
            'data' => $this->binary(),
        ]);
          
        $this->createTable('tag', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'frequency' => $this->integer()->defaultValue(0),
        ]);
          
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'level' => $this->integer()->notNull()->defaultValue(10),
            'status' => $this->integer()->notNull()->defaultValue(10),
            'email' => $this->string(),
            'username' => $this->string()->notNull(),
            'password_hash' => $this->string()->notNull(),
            'auth_key' => $this->string()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'nick' => $this->string()->notNull(),
            'about' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
        $this->dropTable('event');
        $this->dropTable('gallery');
        $this->dropTable('news');
        $this->dropTable('news_attach');
        $this->dropTable('news_has_category');
        $this->dropTable('news_has_tag');
        $this->dropTable('poll');
        $this->dropTable('poll_question');
        $this->dropTable('product');
        $this->dropTable('session');
        $this->dropTable('tag');
        $this->dropTable('user');
    }
}
